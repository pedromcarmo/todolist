Install and run project:

- BACKEND
    - `` cd backend ``
    - `` npm install ``
    - `` cp .env.example .env ``
    - `` npm run start ``

- FRONTEND
    - `` cd frontend ``
    - `` npm install ``
    - `` npm run serve ``

